var setgameComponent = function () {
  var initCarouselSetGame = function () {
    $("#set-game-carousel").owlCarousel({
      items: 4,
      slideBy: 1,
      margin: 5,
      nav: true,
      navText: [
        '<img src="img/prev-carousel.png" />',
        '<img src="img/next-carousel.png" />'
      ],
      responsive: {
        0: {
          items: 1
        },
        480: {
          items: 2
        },
        640: {
          items: 3
        },
        960: {
          items: 4
        },
        1200: {
          items: 4
        }
      }
    });
  }

  return {
    init: function () {
      initCarouselSetGame();
    }
  }
}();

$(document).ready(function () {
  setgameComponent.init();
});
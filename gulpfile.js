var gulp = require('gulp');
var path = require('path');
var less = require('gulp-less');
var watch = require('gulp-watch');
var cleanCSS = require('gulp-clean-css');
var minifyCSS = require('gulp-csso');
var rename = require('gulp-rename');
var header = require('gulp-header');
var pkg = require('./package.json');


/* Prepare banner text */
var banner = ['/**',
    ' * <%= pkg.name %> v<%= pkg.version %>',
    ' * <%= pkg.description %>',
    ' * <%= pkg.author.name %> <<%= pkg.author.email %>>',
    ' */',
    ''].join('\n');

gulp.task('compile-less', function () {
    gulp.src(['./less/jp-style.less'])
        .pipe(less({
            paths: ['./less']
        }))
        .pipe(minifyCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./css/'));
});

gulp.task('watch-less', function () {
    gulp.watch('./less/**/*.less', ['compile-less']);
});

// gulp.task('minify-css', function () {
//     return gulp.src('./css/style.css')
//         .pipe(cleanCSS({
//             debug: true,
//             compatibility: 'ie8'
//         }, function (details) {
//             console.log(details.name + ': ' + details.stats.originalSize);
//             console.log(details.name + ': ' + details.stats.minifiedSize);
//         }))
//         .pipe(rename({
//             suffix: '.min'
//         }))
//         .pipe(header(banner, { pkg: pkg }))
//         .pipe(gulp.dest('./css/'));
// });

// gulp.task('minify-css', function () {
//     gulp.src('./css/style.css')
//         .pipe(minifyCSS())
//         .pipe(gulp.dest('./css/'));
// });

gulp.task('default', ['compile-less', 'watch-less']);

gulp.task('build', ['minify-css']); 